#!/usr/bin/perl

use strict;
use Text::Wrap;

$Text::Wrap::columns = 80;

open FH, "<", "debian/description-list";
my @a = <FH>;
foreach(@a) {
  chomp $_;
  if(length($_) > 80) {
    $_ =~ /^(.{30})(.*)$/;
    my $first = $1;
    my $description = $2;
    my $rest = "                              ";
    print $_ . "\n" foreach(wrap($first, $rest, ($description)));
  } else {
    print $_."\n";
  }
}
